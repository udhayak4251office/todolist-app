// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCOSr3z97MVL8C8Dmf-UQfjRNZru_dsjwE",
  authDomain: "testingapp-95860.firebaseapp.com",
  projectId: "testingapp-95860",
  storageBucket: "testingapp-95860.appspot.com",
  messagingSenderId: "244072161623",
  appId: "1:244072161623:web:ca90666b2370ca8f2f1e12",
  measurementId: "G-T05Y6JCPCB"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { app, db };