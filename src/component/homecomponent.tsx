import { useState } from "react"

export default function HomeComponent() {
    const [list, setList]: any = useState([]);
    const [currentText, setCurrentText] = useState("");

    return <div className="w-full h-screen flex flex-col justify-center items-center">
        <div className="w-[60%]  flex flex-col justify-center items-center">
            <div className="flex w-full justify-center items-center">
                <input type="text" placeholder="Ex. Finish docker deployment" className="w-full border border-teal-400 p-2 text-lg" onChange={(e) => { setCurrentText(e.target.value) }} />
                <button className="p-3 bg-teal-400" onClick={() => { setList([currentText, ...list]) , setCurrentText("")}}>ADD</button>
            </div>
            <div className="flex w-full justify-center items-center flex-col font-left py-2">
                {list.map((text: string, index: number) => {
                    return <p className="text-left text-lg w-full border-b pb-2 pt-1" key={index}>{`${index+1}. ${text}`}</p>
                })}
            </div>
        </div>
    </div>


}